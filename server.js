﻿var https = require('https');
var path = require('path');
var fs = require('fs');
var express = require('express');
var app = express();

var certsPath = path.join(__dirname, 'certs', 'server');

var options = {
    key: fs.readFileSync(path.join(certsPath, 'my-server.key.pem')) //Private key
    , cert: fs.readFileSync(path.join(certsPath, 'my-server.crt.pem')) //Certificate file
    , requestCert: false
    , rejectUnauthorized: true
};

var isFirstClient = false;
var httpsSrv = https.createServer(options, app);
var io = require('socket.io')(httpsSrv);
app.use(express.static(__dirname + '/public'));
httpsSrv.listen(8044);

function log(msg){
    console.log('Got Message' + msg);
}

io.sockets.on('connection', function (socket) {
    var addedUser = false
    socket.on('ice', function (candidate) {
        socket.emit('ice', candidate);
    });
    //The client would need to know if it's the initial client or a client 
    //that's joining 
    socket.on('create or join', function(){
    if (!isFirstClient){
        socket.emit('create');
        isFirstClient=true;
    }
    else 
        socket.emit('join');
    });
    socket.on('sdp', function (sdp) {
        socket.emit('sdp', sdp);
    });

    socket.on('gotmedia', function (stream) { 
        socket.emit('gotmedia', stream);
    });

    socket.on('disconnect', function () {
      console.log('Disconnecting');
    });
});