﻿'use strict';

var sendButton = document.getElementById("sendButton");
var startBtn = document.getElementById("startButton");

// HTML5 <video> elements
var localVideo = document.querySelector('#localVideo');
var remoteVideo = document.querySelector('#remoteVideo');

// Handler associated with 'Send' button
//sendButton.onclick = sendData;

//startBtn.onclick = startBtnClicked;
startBtn.disabled = false;

// Peer Connection ICE protocol configuration (either Firefox or Chrome)
var pc_config = {};

//Session description protocol contraints:
var sdpConstraints = {};
let url = 'https://192.168.1.136:8044';
var socket = io.connect(url, { secure: true });


// Set getUserMedia constraints
var constraints = { video: true, audio: false };

var isInitiator = false;

function errorMsg(msg, error) {
  //errorElement.innerHTML += '<p>' + msg + '</p>';
  console.log(msg);
  if (typeof error !== 'undefined') {
    console.error(error);
  }
}

var localStream, remoteStream;
navigator.mediaDevices.getUserMedia(constraints)
.then((stream)=>{
    let videoTracks = stream.getVideoTracks();
    console.log('Got stream with contraints', constraints);
    console.log('Using video device:' + videoTracks[0].label);
    localVideo.srcObject = stream;
    localStream = stream;
    socket.emit('create or join');
})
.catch((error)=>{
  if (error.name === 'ConstraintNotSatisfiedError') {
    errorMsg('The resolution ' + constraints.video.width.exact + 'x' +
        constraints.video.width.exact + ' px is not supported by your device.');
  } else if (error.name === 'PermissionDeniedError') {
    errorMsg('Permissions have not been granted to use your camera and ' +
      'microphone, you need to allow the page access to your devices in ' +
      'order for the demo to work.');
  }
  errorMsg('getUserMedia error: ' + error.name, error);
});

//We would start only when the other user has 
socket.on('create', ()=>{
    isInitiator = true;
});

socket.on('join', ()=>{
    //Other user has joined the connection 
    createPeerConnection(isInitiator);
});

var pc;

function createPeerConnection(isCaller)
{
    pc = new RTCPeerConnection(pc_config);

    pc.onicecandidate = function (event) { 
        //Once we get the ice candidate send it to the other user 
        //to add ice candidate to remote 
        socket.emit('ice', event);
    };
    //This callback would be called by the remote peer once the remote peer has called setRemoteDescription.
    pc.onaddstream = function (event) {
        console.log("Got Remote Stream");
        remoteVideo.src = URL.createObjectURL(event.stream);
    };
    pc.addStream(localStream);
    if (isCaller) {
        pc.createOffer().then(sdp=>{
            console.log('Got local description from createOffer');
            pc.setLocalDescription(sdp);
            socket.emit('sdp', sdp);
        }).catch(err=>{
            console.log(err);
        });
    }else {
        pc.createAnswer()
        .then(sdp=>{
            console.log('Got local descript from CreateAnswer');
            pc.setLocalDescription(sdp);
            socket.emit('sdp', sdp);
        })
        .catch(err=>{
            console.log(err);
        });
    }
}

socket.on('sdp', function (sdp) {
    pc.setRemoteDescription(new RTCSessionDescription(sdp))
    .then(()=>{
        console.log('Successfully set remote description');
    })
    .catch(err=>{
        console.log("Failed to set remote description with error:" + err);
    });
});

socket.on('ice', function (event) {
    pc.addIceCandidate(new RTCIceCandidate(event.candidate))
    .then(_=>{
        console.log("addIceCandidate was successfully added");
    })
    .catch(err=>{
        console.log("Failed to add IceCandidate with error "+ err);
    });
});